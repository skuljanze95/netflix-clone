import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';

import {
  BrowserRouter as Router,
  Switch,
  Route,

} from "react-router-dom";

import Player from "./player";

ReactDOM.render(
  <React.StrictMode>


    <div>
      <Router>
        <Switch >

          <Route path="/" exact>
            <App />
          </Route>
          <Route path="/:id" exact >
            <Player />

          </Route>

        </Switch>
      </Router>
    </div>




  </React.StrictMode>,
  document.getElementById('root')
);

