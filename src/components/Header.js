import React from 'react'

import '../style/Header.css'

function Header() {
    return (
        <div className="header">
            <img className="header__logo" src="https://image.tmdb.org/t/p/original/wwemzKWzjKYJFfCeiB57q3r4Bcm.svg" alt="" />
            <ul className="header__nav">
                <li className="header__navItem"><p className="active" >Home</p></li>
                <li className="header__navItem"><p >TV Shows</p></li>
                <li className="header__navItem"><p >Movies</p></li>
                <li className="header__navItem"><p >Latest</p></li>
                <li className="header__navItem"><p >My List</p></li>

            </ul>
        </div>
    )
}

export default Header
