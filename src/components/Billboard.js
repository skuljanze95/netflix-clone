import React, { useState, useEffect } from 'react'
import axios from '../axios'
import '../style/Billboard.css'
import requests from "../requests"

function Billboard() {

    const [movie, setMovie] = useState([]);

    const base_url = "https://image.tmdb.org/t/p/original"

    useEffect(() => {

        async function fetchData() {

            const request = await axios.get(requests.fetchTrending)
            setMovie(request.data.results[2]);
            //console.log(request.data.results[2])

            return request;
        }
        fetchData()
    }, [])

    return (

        <div className="billboard">
            <div className="shadow" ></div>
            <img className="billboard__image" src={`${base_url}${movie.backdrop_path}`} alt="" />
            <div className="billboard__content">
                <h1 className="billboard__content__tittle" >{movie.title}</h1>
                <p className="billboard__content__overview" >{movie.overview}</p>
                <div className="billboard__buttons">
                    <button className="billboard__content__play"><svg viewBox="0 0 24 24"><path d="M6 4l15 8-15 8z" fill="#141414"></path></svg><p>Play</p></button>
                    <button className="billboard__content__info" ><svg viewBox="0 0 24 24"><path d="M22 12c0 5.523-4.477 10-10 10S2 17.523 2 12 6.477 2 12 2s10 4.477 10 10zm-2 0a8 8 0 0 0-8-8 8 8 0 0 0-8 8 8 8 0 0 0 8 8 8 8 0 0 0 8-8zm-9 6v-7h2v7h-2zm1-8.75a1.21 1.21 0 0 1-.877-.364A1.188 1.188 0 0 1 10.75 8c0-.348.123-.644.372-.886.247-.242.54-.364.878-.364.337 0 .63.122.877.364.248.242.373.538.373.886s-.124.644-.373.886A1.21 1.21 0 0 1 12 9.25z" fill="white"></path></svg><p>More Info</p></button>
                </div>
            </div>
        </div>
    )
}

export default Billboard
