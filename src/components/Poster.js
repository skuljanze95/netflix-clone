import React, { useState, useEffect } from 'react'
import axios from '../axios'
import '../style/Row.css'


import { Link } from "react-router-dom";


function Poster({ media, movie_id, backdrop, title, isTall }) {

    const [image, setImage] = useState([]);

    const base_url = "https://image.tmdb.org/t/p/original"

    const [isShown, setIsShown] = useState(false);



    useEffect(() => {


        async function fetchData() {

            const request = await axios.get(`https://api.themoviedb.org/3/${media}/${movie_id}?api_key=85ad6c9c1e79b127026e8d2270b8110d&append_to_response=images&include_image_language=en`)

            if (!isTall) {
                if (request.data.backdrop_path === null) {

                } else {
                    if (request.data.images.backdrops[0] === undefined) {
                        setImage(backdrop)
                    } else {
                        setImage(request.data.images.backdrops[0].file_path)
                    }

                }
            } else {
                if (request.data.poster_path === null) {

                } else {

                    setImage(request.data.poster_path)


                }

            }

            return request;
        }
        fetchData()
    }, [movie_id, backdrop, media, isTall])

    return (

        <div className="row__poster" href="">
            <div className={`row__shadow ${isShown ? "isShown" : ""}`}></div>
            <h1 className={`row__poster__title ${isShown ? "isShown" : ""}`}>{title}</h1>
            <Link to={`/${movie_id}`} > <img
                onMouseEnter={() => { setIsShown(true) }}
                onMouseLeave={() => { setIsShown(false) }}
                className={`row__image${isTall ? "__isTall" : ""}`} src={`${base_url}${image}`} alt="" />
            </Link>
        </div>



    )
}

export default Poster
