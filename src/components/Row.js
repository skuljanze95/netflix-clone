import React, { useState, useEffect } from 'react'
import axios from '../axios'
import '../style/Row.css'


import Poster from "./Poster"




function Row({ title, fetchUrl, media, isTall }) {

    const [movies, setMovies] = useState([]);





    useEffect(() => {

        async function fetchData() {

            const request = await axios.get(fetchUrl)
            setMovies(request.data.results);

            return request;
        }
        fetchData()
    }, [fetchUrl])

    return (
        <div>

            <h1 className="row__genre">{title}</h1>
            <div className="row">
                <div className={`row__posters${isTall ? "__isTall" : ""}`}>

                    {movies.map(movie => (



                        <Poster key={movie.id} movie_id={movie.id} backdrop={movie.backdrop_path} title={movie.title == null ? movie.name : movie.title} media={media} isTall={isTall} />




                    ))};

                </div>
            </div>

        </div>

    )
}

export default Row
