import React from "react";

import Row from "./components/Row"
import requests from "./requests"
import Header from './components/Header'
import Billboard from './components/Billboard'

import "./App.css";




function App() {
  return (<div className="App">



    <Header />
    <Billboard />
    <Row title="Trending" fetchUrl={requests.fetchTrending} media="movie" isTall={false} />
    <Row title="Action Movies " fetchUrl={requests.fetchActionMovies} media="movie" isTall={false} />
    <Row title="Comedy TV Shows" fetchUrl={requests.fetchComedyTvShows} media="tv" isTall={false} />
    <Row title="Netflxi Originals" fetchUrl={requests.fetchNetflixOriginals} media="tv" isTall={true} />
    <Row title="Documentaries" fetchUrl={requests.fetchDocumentaries} media="movie" isTall={false} />
    <Row title="Romance Movies" fetchUrl={requests.fetchRomanceMovies} media="movie" isTall={false} />
    <Row title="Animation Tv Shows" fetchUrl={requests.fetchAnimationTvShows} media="tv" isTall={false} />

  </div>
  );
}


export default App;
